const request = require('request');
const argumentos = process.argv.slice(2);
const url = 'https://pokeapi.co/api/v2/pokemon/';
const fs = require('fs');

for (let i of argumentos) {
  if (parseInt(i) < 1 || parseInt(i) > 893) {
    throw new Error('O numero não faz parte da lista');
  }
}

for (let i of argumentos) {
  request(url + i, function (err, res, body) {
    if (!err) {
      const objeto = JSON.parse(body);
      const pokemon = {
        id: objeto.id,
        nome: objeto.name,
        sprite: objeto.sprites.front_default,
        altura: objeto.height,
        peso: objeto.weight,
        tipo: objeto.types,
      };
      //console.log(pokemon);

      const uri = 'dados/pokemon' + pokemon.id + '.json';
      const dados = JSON.stringify(pokemon);
      const options = {
        encoding: 'utf-8',
        flag: 'w',
      };
      fs.writeFileSync(uri, dados, options);

      console.log(`O pokemon ${pokemon.nome} foi salvo com sucesso!`);
    }
  });
}
