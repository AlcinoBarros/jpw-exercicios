/*Exercício 05: Olá NPM
Crie pacote usando npm e implemente um script que receba argumentos, como node 
index.js 1 2 3 4 5 6. O script deve retornar a soma total apenas dos argumentos 
divisíveis por 2. */
const argumento = process.argv.slice(2);

const soma = argumento.reduce(function (valorAnterior, item) {
  if (item % 2 == 0) {
    return valorAnterior + parseInt(item);
  } else {
    return valorAnterior;
  }
});

console.log(soma);
