import React from 'react';
import ArCondicionado from './ArCondicionado';

export default class Ambient extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      temperatura: 20,
    };
  }

  modificarTemperatura = (valor) => {
    this.setState({
      temperatura: this.props.temperatura + valor,
    });
  };

  render() {
    let lista = [];
    for (let i = 0; i < 3; i++) {
      lista.push(<ArCondicionado modificarTemperatura={this.modificarTemperatura} key={i}></ArCondicionado>
      );
    }

    return (
      <main>
        <div>Temperatura Ambiente: {this.state.temperatura} graus</div>
        <div style={{ display: 'flex' }}>{lista}</div>
      </main>
    );
  }
}
