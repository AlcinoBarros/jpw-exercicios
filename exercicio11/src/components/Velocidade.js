import React from 'react';

export default class Velocidade extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      velocidade: this.props.valor,
    };
  }

  alteraValor = (evento) => {
    let valor = parseInt(evento.target.value);
    if (valor >= 0 && valor < 3) {
      this.setState({ velocidade: valor });
      this.props.alterar(valor);
    }
  };

  render() {
    return (
      <div style={{ backgroundColor: '#CC00CC' }}>
        <input
          type="number"
          max="3"
          min="0"
          onChange={this.alteraValor}
          value={this.props.valor}
        ></input>
      </div>
    );
  }
}
