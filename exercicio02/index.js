function gerarPartida() {
  let partida = times();

  partida.golsCasa = parseInt((Math.random() * 100) % 4);
  partida.golsVisitante = parseInt((Math.random() * 100) % 4);

  setTimeout(function () {
    let div = document.createElement('div');
    let section = document.querySelector('#listaPartidas');
    section.appendChild(div);

    div.textContent =
      partida.timeCasa +
      ' ' +
      partida.golsCasa +
      ' X ' +
      partida.golsVisitante +
      ' ' +
      partida.timeVisitante;
  }, 1000);
}

function times() {
  let timeCasaValor = document.querySelector('#timeCasa').value;
  let timeVisitanteValor = document.querySelector('#timeVisitante').value;

  let partida = {
    timeCasa: timeCasaValor,
    timeVisitante: timeVisitanteValor,
  };

  return partida;
}

document.querySelector('#botaoGerar').addEventListener('click', gerarPartida);
